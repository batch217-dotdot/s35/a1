const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// Section - MongoDB connection
// syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.ncobv0c.mongodb.net/B217_to_do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Section - Mongoose Schemas
// it determines the structure of our documents to be stored in the database
// it acts as our data blueprint and guide

const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},
	status : {
		type : String,
		default: "pending"
	}
});

// user schema
const userSchema = new mongoose.Schema({
	username : {
		type : String,
		required: [true, "Username is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	}
})

// Section - Models
const Task = mongoose.model("Task", taskSchema);

// user model
const User = mongoose.model("User", userSchema);

// Section - Creation of to do list routes

app.use(express.json());
// allows your app to read data from forms
app.use(express.urlencoded({extended: true}));

// Creating a new task
// Business logic
/*
1. add a functionality to check if there are duplicte tasks
	-if the task already exist in the database, we return an error
	-if the task doesnt exsit in the database, we add it in the database
2. the task data will be coming from the request's body
3. create a new Task object with a "name" field/property
4. the "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		}
		else{
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.log(saveErr);
				}
				else{
					return res.status(201).send("New task created.")
				}
			})
		}
	})
})

// user post method
app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("User already exist.")
		}
		else{
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})

			newUser.save((saveErr, saveUser) => {
				if(saveErr){
					return console.log(saveErr);
				}
				else{
					return res.status(201).send("New user registered.")
				}
			})
		}
	})
})

// Get method
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// users get method
app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`));